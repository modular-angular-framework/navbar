import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppComponent } from './app.component';
import { EntryComponent } from './entry/entry.component';
import { EntryModule } from './entry/entry.module';

/**
 * AppModule is used for development and integration testing.
 * It is not part of the exported bundle.
 */
@NgModule({
  declarations: [AppComponent, EntryComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([], { useHash: true }),
    StoreModule.forRoot({}),
    StoreDevtoolsModule.instrument(),

    EntryModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
