import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { DecrementCounter, IncrementCounter } from './actions';
import { State } from './state';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent {
  constructor(private store: Store<State>) {}

  counter: Observable<number> = this.store.select('counter');

  onIncrement() {
    this.store.dispatch(new IncrementCounter());
  }

  onDecrement() {
    this.store.dispatch(new DecrementCounter());
  }
}
