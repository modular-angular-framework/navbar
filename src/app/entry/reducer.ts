import {
  CounterActions,
  DECREMENT_COUNTER,
  INCREMENT_COUNTER
} from './actions';
import { initialState, State } from './state';

export function counterReducer(
  state = initialState,
  action: CounterActions
): State {
  switch (action.type) {
    case INCREMENT_COUNTER: {
      return state + 1;
    }

    case DECREMENT_COUNTER: {
      return state - 1;
    }

    default:
      return state;
  }
}
