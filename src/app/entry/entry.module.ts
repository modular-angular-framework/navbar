import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ClrVerticalNavModule } from '@clr/angular';
import { StoreModule } from '@ngrx/store';
import { EntryComponent } from './entry.component';
import { counterReducer } from './reducer';

/**
 * EntryModule is the entry point for this bundled dashboard.
 * It is important to name your enty point the EntryModule
 * because the @hack/core's module loader will references it
 * by EntryModule.
 *
 * @hack/navbar's EntryModule has a special provider to
 * resolve it's EntryComponent without having to router based
 * lazy loading (which doesn't appear possible at the moment
 * due to issues with named router-outlets and loadChildren)
 */
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([]),
    ClrVerticalNavModule,
    StoreModule.forFeature('counter', counterReducer)
  ],
  declarations: [EntryComponent],
  entryComponents: [EntryComponent],
  providers: [{ provide: 'Entry', useValue: EntryComponent }]
})
export class EntryModule {}
