# Dashboard Beta

## Modular Angular Framework - Demo Module

Demo/test module for the Modular Angular Framework.

## Credits

- David Lynam's Rebelcon 2018 talk [Our Journey To Modular UI Development](http://rebelcon.io/talks/david-lynam-journey-to-modular-ui/#talk)
- [Here is what you need to know about dynamic components in Angular](https://blog.angularindepth.com/here-is-what-you-need-to-know-about-dynamic-components-in-angular-ac1e96167f9e)
